//*******************************************************
// Calculator.java
// created by Sawada Tatsuki on 2016/08/08.
// Copyright © 2016 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* Javaアプリ処女作 */

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javax.script.*;

public class Calculator extends Application {
    static String str = new String("");
    static Label lbTop = new Label(str);

    @Override
    public void start(Stage primaryStage){
	BorderPane root = new BorderPane();

	/*+++++  Top部分 +++++*/
	root.setTop(lbTop);
        lbTop.setPrefWidth(240); lbTop.setPrefHeight(80);
	lbTop.setStyle("-fx-padding:12px;-fx-background-color:#111;-fx-font-size:20px;-fx-text-fill:#fff");

	/*+++++  Center部分 +++++*/
	String[] fkey = {"C","DEL","sin","cos","tan","log10","(",")"};
	final int FBTNUM = fkey.length;
	Button[] fbutton = new Button[FBTNUM];
	String[] fcolor = new String[FBTNUM];
	for(int i=0;i<FBTNUM;i++){
	    if(0<=i && i<=1) fcolor[i]="#a01";
	    if(2<=i && i<=5) fcolor[i]="#444";
	    if(6<=i && i<=7) fcolor[i]="#ccc";
	}
	for(int i=0;i<FBTNUM;i++){
	    fbutton[i]=new Button(fkey[i]);
	    fbutton[i].setPrefHeight(40);
	    if(0<=i && i<=1 || 6<=i && i<=7)
		fbutton[i].setPrefWidth(120);
	    else if(2<=i && i<=5)
		fbutton[i].setPrefWidth(60);
	    fbutton[i].setStyle("-fx-font-size:12px;-fx-base:"+fcolor[i]);
	    if(0 == i)
		fbutton[i].setOnAction(event->lbTop.setText(str = ""));
	    if(1 == i)
		fbutton[i].setOnAction(event->lbTop.setText(str = str.substring(0,str.length()-1) ));
	    if(2<=i && i<=5){
		fbutton[i].setOnAction(new MyButtonHandler(fkey[i]));
	    }
	    if(6<=i && i<=7){
		final int j = i;
		fbutton[i].setOnAction(event->lbTop.setText(str = str.concat(fkey[j])));
	    }	
	}
	FlowPane fp = new FlowPane();
	root.setCenter(fp);
	fp.getChildren().addAll(fbutton);

	/*+++++  Bottom部分 +++++*/
        GridPane gridPane=new GridPane();
	root.setBottom(gridPane);
	String[] key = {"0",".","=","1","2","3","4","5","6","7","8","9","+","-","*","/"};
	final int BTNUM = key.length;;
        Button[] button=new Button[BTNUM];
	String[] color = new String[BTNUM];
	for(int i=0;i<BTNUM;i++){
	    if(i==2)color[i]="orange";
	    else if(0<=i&&i<=11)color[i]="#aacc3b";
	    if(12<=i&&i<=15)color[i]="#2e0000";
	}
	for(int i=0;i<BTNUM;i++){
	    button[i]=new Button(key[i]);
	    button[i].setPrefHeight(60); button[i].setPrefWidth(60);
	    button[i].setStyle("-fx-font-size:24px;-fx-base:"+color[i]);
	    if(0<=i && i<=11)
		gridPane.add(button[i],i%3,3-i/3);
	    else if(12<=i && i<=15)
		gridPane.add(button[i],4,3-(i-12));
	    if(i==2){
		button[i].setOnAction(new MyButtonHandler());
		continue;
	    }
	    final int j = i;
	    button[i].setOnAction(event->lbTop.setText(str = str.concat(key[j])));
	}

	/*+++++  Stage設定 +++++*/
	Scene scene = new Scene(root, 230, 430);
	primaryStage.setTitle("電卓");
	primaryStage.setScene(scene);
	primaryStage.show();
	primaryStage.setResizable(false);
    }    

    class MyButtonHandler implements EventHandler<ActionEvent> {
	String op = new String();

	MyButtonHandler(){};

	MyButtonHandler(String op){
	    this.op = op;
	}

        @Override
        public void handle(ActionEvent event) {
	    try {
		ScriptEngineManager factory = new ScriptEngineManager();
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		str = String.valueOf(engine.eval(str));
		double val = Double.parseDouble(str);
		if(op == "sin")
		    val = Math.sin(val);
		if(op == "cos")
		    val = Math.cos(val);
		if(op == "tan")
		    val = Math.tan(val);
		if(op == "log10")
		    val = Math.log10(val);
		str = String.valueOf(val);

	    } catch (ScriptException e) {
		throw new RuntimeException(e);
	    }
	    lbTop.setText(str = str.concat(""));
        }
    }
}
